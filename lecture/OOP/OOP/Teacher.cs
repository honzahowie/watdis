﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Teacher : Person, ICommunicable, ICommunicableNew
    {
        private string[] classes = new string[5];




        public Teacher(string teacherName, int age):base(teacherName, age, 300, 300)
        {
            job = "ucitel";
        }

        public void SetClass(string name, int id)
        {
            classes[id] = name;
        }

        public string GetFirstClass()
        {
            return classes[0];
        }

        public string Communicate()
        {
            return "Komunikuji";
        }

        public string CommunicateNew()
        {
            throw new NotImplementedException();
        }
    }
}
