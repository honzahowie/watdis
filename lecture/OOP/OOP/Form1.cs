﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;



            Person[] people = new Person[3];
            people[0] = new Person("David", 23, 20, 20);
            people[1] = new Person("Jirka", 23, 100, 100);
            people[2] = new Person("Honza", 23, 100, 250);

            foreach (Person item in people)
            {
                item.Draw(kp);
            }

            people[0].SayHi();
            
            Person pepa = new Person("Pepa", 20, 200, 200);
            pepa.Draw(kp);
            pepa.SayHi();
            pepa.SayJob();

            Teacher t = new Teacher("Cigi", 100);
            t.Draw(kp);
            t.SayHi();
            t.SayJob();
            t.SetClass("matematika", 0);
            string text = t.Communicate();

            Person pt = t;
            ICommunicable ct = t;
            ct.Communicate();

        }        
    }
}
