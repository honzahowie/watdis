﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP
{
    class Person
    {
        private float x, y;
        private string name;
        private int age;
        protected string job;


        public Person(string name, int age, float x, float y)
        {
            this.name = name;
            this.age = age;
            this.x = x;
            this.y = y;
        }

        public void Draw(Graphics kp)
        {
            //hlava
            kp.FillEllipse(Brushes.Blue, x, y, 25, 25);

            //telo
            Pen p = new Pen(Color.Blue, 5);
            kp.DrawLine(p, x + 12.5f, y + 25, x + 12.5f, y + 50);

            //ruce            
            kp.DrawLine(p, x + 12.5f, y + 25, x, y + 45);            
            kp.DrawLine(p, x + 12.5f, y + 25, x + 25, y + 45);

            //nohy
            kp.DrawLine(p, x + 12.5f, y + 50, x, y + 75);
            kp.DrawLine(p, x + 12.5f, y + 50, x + 25, y + 75);

            //jmeno
            DrawName(kp);
        }

        private void DrawName(Graphics kp)
        {
            //jmeno
            Font f = new Font("Arial", 13);
            Brush b = new SolidBrush(Color.Blue);
            kp.DrawString(name, f, b, x - 12.5f, y - 20);
        }

        public void SayHi()
        {
            MessageBox.Show("Ahoj, ja jsem " + name);
        }
        public void SayJob()
        {
            MessageBox.Show($"{name} pracuje jako {job}");
        }


    }
}
