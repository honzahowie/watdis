﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CyklyARekurze
{
    class Program
    {
        static void Main(string[] args)
        {
            //suma(6) = 6+5+4+3+2+1;
            //SUMA
            Console.WriteLine(Suma(6));

            //factorial(6) = 
            //FAKTORIAL
            Console.WriteLine(Factorial(6));

            try
            {
                Console.WriteLine(FactorialRec(20));
            }
            catch (OverflowException e)
            {

                Console.WriteLine(e.Message);
            }

            Console.WriteLine(SumaRecN3(6));
            Console.WriteLine(SumaRecN3(6));
            Console.WriteLine(Euler(4));
           
            
            Console.ReadKey();
        }

        static int FactorialRec(int number)
        {
            Console.WriteLine("Volani pro cislo {0}", number);
            if (number < 2) return 1;

            int result = 0;
            checked
            {
                result = number * FactorialRec(number - 1);
            }
            
            Console.WriteLine("Konec volani pro cislo {0}, result je {1}", number, result);
            return result;
        }
        static int Factorial(int number)
        {
            int i = 1;
            int fact = 1;

            while (i <= number)
            {
                fact *= i;
                i++;
            }

            return fact;
        }
       
       static int SumaRec(int number)
        {
            if (number < 2) return 1;
            int result = number + SumaRec(number - 1);

            return result;
        }
        static double SumaRecN3 (int number)
        {
            if (number < 2) return 1;
            return Math.Pow(number, 3) + SumaRecN3(number - 1);            
        }
        static int Suma(int number)
        {
            int result = 0;
            for (int i = 1; i <= number; i++)
            {
                result += i;
            }

            return result;
        }

        static double Euler(int number)
        {
            if (number < 1) return 1;

            return (1D / FactorialRec(number)) + Euler(number - 1);
        }
    }
}
