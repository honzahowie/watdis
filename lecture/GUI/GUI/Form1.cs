﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Form1 : Form
    {
        int[] arr;
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonExample_Click(object sender, EventArgs e)
        {
            string text = textBoxExample.Text;

            labelExample.Text = text;

            textBoxExample.Text = " ";

        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            arr = new int[20];
            Random rnd = new Random();

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(0, 100);
            }

            textBoxVstup.Text = string.Join(" ", arr);
        }

        private void buttonSort_Click(object sender, EventArgs e)
        {
            InsertSort(arr);

            textBoxVstup.Text = string.Join(" ", arr);
        }

        private void InsertSort(int[] arr)
        {
            for (int i = 0; i < arr.Length - 1; i++)
            {
                int j = i + 1;
                int tmp = arr[j];
                while (j > 0 && tmp < arr[j-1])
                {
                    arr[j] = arr[j - 1];
                    j--;
                }
                arr[j] = tmp;
            }
                       
        }
    }
}
