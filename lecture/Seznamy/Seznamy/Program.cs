﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seznamy
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] pole = new int[10];

            List<int> seznam = new List<int>();
            seznam.Add(5);
            seznam.Add(9);
            seznam.Add(9);
            seznam.Add(3);

            seznam.Insert(0, 8);

            Console.WriteLine(seznam.Count);
            PrintList(seznam);

            seznam.Remove(9);
            seznam.RemoveAt(0);

            PrintList(seznam);

            seznam.Clear();
            Console.WriteLine(seznam.Count);

            Random rnd = new Random();
            int[] arr = new int[10];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(0, 99);
            }

            List<int> suda = new List<int>();
            List<int> licha = new List<int>();

            foreach (int item in arr)
            {
                if (item % 2 == 0)
                    suda.Add(item);
                else
                    licha.Add(item);                
            }

            List<int> prvocisla = new List<int>();
            List<int> neprvocisla = new List<int>();

            foreach (int item in arr)
            {
                if (IsPrime(item))
                    prvocisla.Add(item);
                else
                    neprvocisla.Add(item);
            }

            Console.Write("Suda cisla: ");
            PrintList(suda);
            Console.Write("Licha cisla: ");
            PrintList(licha);


            List<int> listFromArray = arr.ToList();
            PrintList(arr.ToList());




            Console.ReadKey();
        }
        static bool IsPrime(int x)
        {
            bool isNumberPrime= true;

            return isNumberPrime;
        }
        static void PrintList(List<int> list)
        {
            foreach (int item in list)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }
    }
}
