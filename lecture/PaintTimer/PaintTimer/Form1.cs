﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintTimer
{
    public partial class Form1 : Form
    {
        int x = 0;
        int y = 0;
        public Form1()
        {
            InitializeComponent();
        }
                
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;

            //kp.DrawRectangle(Pens.Red, 54,54,50,50);
            kp.FillEllipse(Brushes.Black, x+5, y, 40,40);
            kp.FillRectangle(Brushes.Black, x, y+40, 50, 50);              
        }

        private void buttonMove_Click(object sender, EventArgs e)
        {
            x += 10;

            Refresh();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            x += 10;

            Refresh();
        }

        private void timerSwitch_Click(object sender, EventArgs e)
        {
            
            timer1.Enabled = !timer1.Enabled;
            Refresh();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
         
            switch(e.KeyCode)
            {
                case Keys.W:
                    y -= 10;
                    break;
                case Keys.S:
                    y += 10;
                    break;
                case Keys.A:
                    x -= 10;
                    break;
                case Keys.D:
                    x += 10;
                    break;
            }
            Refresh();
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            //vicerozmerne pole 2D
            int[,] array2d = new int[5,6];

            array2d[0, 0] = 1;
            array2d[1, 5] = 2;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array2d.GetLength(0); i++)
            {
                for (int j = 0; j < array2d.GetLength(1); j++)
                {
                    sb.Append(array2d[i, j]);
                    sb.Append(" ");

                }
                sb.AppendLine();
            }


            richTextBox1.Text = sb.ToString();

            //pole polí
            int[][] array2d2 = new int[5][];

            array2d2[0] = new int[9];
            array2d2[1] = new int[9];
            array2d2[2] = new int[9];

            int k = array2d2[0][0];
        }
    }
}
