﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZakladniStruktury
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zakladni ridici struktury
            //sekvence, selekce a iterace

            //Sekvence
            int number = 5;
            number += 3;
            // stejné jako number = number + 3
            Console.WriteLine(number);

            //Selekce - podmínky, větvení
            // == !=
            //< > <= >=
            //!


            //&& - and / a
            // || - or / nebo
            bool success = false;
            if (5 > 3 && success);
            {
                Console.WriteLine("plati");
            }
           
            bool success2 = false;
            //   true     false
            if (5 > 3 || success2) ;
            {
                Console.WriteLine("plati");
            }

            switch (success)
            {
                case true:
                    Console.WriteLine("success is true");
                    break;
                default:
                    break;
            }

            //iterace / cykly
            //for, while, do while a foreach

            //for - pevný počet opakování
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine();
            }

            //while
            int l = 10;
            while (l < 10)
            {
                Console.WriteLine(l);
                l++;
            }
            
            //do while
            int j = 10;
            do
            {
                Console.WriteLine(j);
                j++;
            } while (j < 10);

            //foreach
            int[] array = new int[] { 5, 9, 10, 12 };
            foreach (int item in array)
            {
                Console.WriteLine(item);
            }

            for (int i = 0; i< array.Length; i++)
            {
                array[i] += 1;
                //Console.WriteLine(array[i]);
            }
            
            foreach (int item in array)
            {
                Console.WriteLine(item);
            }         

            Console.ReadKey();
        }
    }
}
