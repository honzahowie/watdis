﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafika
{
    public partial class FormElipsa : Form
    {
        float width = 0;
        float height = 0;

        public FormElipsa()
        {
            InitializeComponent();
        }

        private void buttonVykresli_Click(object sender, EventArgs e)
        {
            width = Convert.ToSingle(textBoxWidth.Text);
            height = Convert.ToSingle(textBoxHeight.Text);

            panel1.Refresh();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Pen p = new Pen(Color.Red, 2);
            g.DrawEllipse(p, 50, 50, width, height);
        }
    }
}
