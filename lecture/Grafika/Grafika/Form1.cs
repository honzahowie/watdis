﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafika
{
    public partial class Form1 : Form
    {
        Graphics g;
        public Form1()
        {
            InitializeComponent();

            g = panel1.CreateGraphics();
        }

        private void buttonElipsa_Click(object sender, EventArgs e)
        {
            Form form = new FormElipsa();
            form.Show();
        }

        private void buttonDraw_Click(object sender, EventArgs e)
        {
            Form form = new DrawingForm();
            form.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Random rnd = new Random();

            //tvar
            Shape shape = (Shape)rnd.Next(0, 4);

            //barva
            int red = rnd.Next(0, 256);
            int green = rnd.Next(0, 256);
            int blue = rnd.Next(0, 256);
            Color color = Color.FromArgb(red, green, blue);

            //velikost
            int width = rnd.Next(0, 100);
            int height = rnd.Next(0, 100);

            //pozice
            int x = rnd.Next(0, panel1.Width - width);
            int y = rnd.Next(0, panel1.Height - height);

            //
            bool isFill = rnd.Next(0, 2) == 0;

            Pen pen = new Pen(color, 2);
            Brush brush = new SolidBrush(color);
           
            switch (shape)
            {
                case Shape.Ellipse:
                    if (isFill)
                    {
                        g.FillEllipse(brush, x, y, width, height);
                    }
                    else
                    {
                        g.DrawEllipse(pen, x, y, width, height);
                    }                    
                    break;
                case Shape.Rectangle:
                    if (isFill)
                    {
                        g.FillRectangle(brush, x, y, width, height);
                    }
                    else
                    {
                        g.DrawRectangle(pen, x, y, width, height);
                    }
                    break;
                case Shape.Line:
                    g.DrawLine(pen, x, y, x+width, y+height);
                    break;
                case Shape.String:
                    Font font = new Font("Arial", 14);
                    g.DrawString("GYMVOD", font, brush, x, y);
                    break;
                default:
                    break;
            }

        }
        enum Shape
        {
            Ellipse, Rectangle, Line, String
        }
    }
}
