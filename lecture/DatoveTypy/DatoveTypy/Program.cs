﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatoveTypy
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "nazev";
            int number = 10;

            float flt = 1F / 3;
            float flt1 = 1 / 3;
            float flt2 = (float)1 / 3;
            double dbl = 1D / 3;
            double dbl1 = (double)1 / 3;
            decimal dcm = 1M / 3;

            Console.Write("float flt = 1F / 3: ");
            Console.WriteLine(flt);
            Console.Write("float flt1 = 1 / 3: ");
            Console.WriteLine(flt1);
            Console.Write("float flt2 = (float)1 / 3: ");
            Console.WriteLine(flt2);
            Console.Write("double dbl = 1D / 3: ");
            Console.WriteLine(dbl);
            Console.Write("double dbl1 = (double)1 / 3: ");
            Console.WriteLine(dbl1);
            Console.Write("decimal dcm = 1M / 3: ");
            Console.WriteLine(dcm);

            Console.WriteLine("Konverze");
            //implicitní konverze
            int i = 10;
            long l = i;

            int s = 60;
            string msg = "Soucet je" + s;

            //explicitní koverze
            int parsedNumber = int.Parse("5");
            int convertedNumber = Convert.ToInt32("6");
            double doubleNumber = 5.6;
            int intNumber = (int)doubleNumber;
            Console.WriteLine(intNumber);

            Console.WriteLine("Overflow");
            //Overflow
            int hodnotaInt = 300;
            byte hodnotaByte = (byte)hodnotaInt;
            sbyte hodnotaSbyte = (sbyte)hodnotaInt;
            Console.WriteLine(hodnotaByte);            
            Console.WriteLine(hodnotaSbyte);



            Console.WriteLine("Trida Convert");
            try
            {
                //Trida Cinvert
                int intToConvert = 300;
                byte convertedByte = Convert.ToByte(intToConvert);
                Console.WriteLine(convertedByte);
            }
            catch (OverflowException e)
            { 
                Console.WriteLine("Chytil jsem vyjimku. Neco je spatne.");
            }

            
            Console.ReadKey();






        }
    }
}
