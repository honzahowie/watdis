﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prace1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void otevřítToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                textBoxSoubor.Text = openFileDialog1.FileName;
            }
        }

        private void buttonZapsat_Click(object sender, EventArgs e)
        {
            StreamWriter output = new StreamWriter(openFileDialog1.FileName, true);
            output.WriteLine(textBoxJmeno.Text);
            output.Close();      
        }

        private void buttonVypsat_Click(object sender, EventArgs e)
        {
            StreamReader input = new StreamReader(openFileDialog1.FileName);
            string text = input.ReadToEnd();
            richTextBoxtVystup.Text = text;
            input.Close();
        }

        private void buttonPromazat_Click(object sender, EventArgs e)
        {            
            StreamWriter output = new StreamWriter(openFileDialog1.FileName);
            output.Write("");
            output.Close();
            richTextBoxtVystup.Text = "";
        }
    }
}
