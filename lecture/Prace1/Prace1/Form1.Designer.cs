﻿namespace Prace1
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.souborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otevřítToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.textBoxSoubor = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxJmeno = new System.Windows.Forms.TextBox();
            this.buttonZapsat = new System.Windows.Forms.Button();
            this.buttonVypsat = new System.Windows.Forms.Button();
            this.buttonPromazat = new System.Windows.Forms.Button();
            this.richTextBoxtVystup = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.souborToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(381, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // souborToolStripMenuItem
            // 
            this.souborToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.otevřítToolStripMenuItem});
            this.souborToolStripMenuItem.Name = "souborToolStripMenuItem";
            this.souborToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.souborToolStripMenuItem.Text = "Soubor";
            // 
            // otevřítToolStripMenuItem
            // 
            this.otevřítToolStripMenuItem.Name = "otevřítToolStripMenuItem";
            this.otevřítToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.otevřítToolStripMenuItem.Text = "Otevřít";
            this.otevřítToolStripMenuItem.Click += new System.EventHandler(this.otevřítToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // textBoxSoubor
            // 
            this.textBoxSoubor.Location = new System.Drawing.Point(13, 28);
            this.textBoxSoubor.Name = "textBoxSoubor";
            this.textBoxSoubor.Size = new System.Drawing.Size(356, 20);
            this.textBoxSoubor.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Jméno:";
            // 
            // textBoxJmeno
            // 
            this.textBoxJmeno.Location = new System.Drawing.Point(59, 59);
            this.textBoxJmeno.Name = "textBoxJmeno";
            this.textBoxJmeno.Size = new System.Drawing.Size(206, 20);
            this.textBoxJmeno.TabIndex = 3;
            // 
            // buttonZapsat
            // 
            this.buttonZapsat.Location = new System.Drawing.Point(59, 85);
            this.buttonZapsat.Name = "buttonZapsat";
            this.buttonZapsat.Size = new System.Drawing.Size(206, 23);
            this.buttonZapsat.TabIndex = 4;
            this.buttonZapsat.Text = "Zapiš jméno do souboru";
            this.buttonZapsat.UseVisualStyleBackColor = true;
            this.buttonZapsat.Click += new System.EventHandler(this.buttonZapsat_Click);
            // 
            // buttonVypsat
            // 
            this.buttonVypsat.Location = new System.Drawing.Point(59, 115);
            this.buttonVypsat.Name = "buttonVypsat";
            this.buttonVypsat.Size = new System.Drawing.Size(119, 23);
            this.buttonVypsat.TabIndex = 5;
            this.buttonVypsat.Text = "Vypiš obsah souboru";
            this.buttonVypsat.UseVisualStyleBackColor = true;
            this.buttonVypsat.Click += new System.EventHandler(this.buttonVypsat_Click);
            // 
            // buttonPromazat
            // 
            this.buttonPromazat.Location = new System.Drawing.Point(185, 115);
            this.buttonPromazat.Name = "buttonPromazat";
            this.buttonPromazat.Size = new System.Drawing.Size(80, 23);
            this.buttonPromazat.TabIndex = 6;
            this.buttonPromazat.Text = "Promazat";
            this.buttonPromazat.UseVisualStyleBackColor = true;
            this.buttonPromazat.Click += new System.EventHandler(this.buttonPromazat_Click);
            // 
            // richTextBoxtVystup
            // 
            this.richTextBoxtVystup.Location = new System.Drawing.Point(13, 144);
            this.richTextBoxtVystup.Name = "richTextBoxtVystup";
            this.richTextBoxtVystup.Size = new System.Drawing.Size(356, 294);
            this.richTextBoxtVystup.TabIndex = 8;
            this.richTextBoxtVystup.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 450);
            this.Controls.Add(this.richTextBoxtVystup);
            this.Controls.Add(this.buttonPromazat);
            this.Controls.Add(this.buttonVypsat);
            this.Controls.Add(this.buttonZapsat);
            this.Controls.Add(this.textBoxJmeno);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxSoubor);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem souborToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otevřítToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox textBoxSoubor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxJmeno;
        private System.Windows.Forms.Button buttonZapsat;
        private System.Windows.Forms.Button buttonVypsat;
        private System.Windows.Forms.Button buttonPromazat;
        private System.Windows.Forms.RichTextBox richTextBoxtVystup;
    }
}

