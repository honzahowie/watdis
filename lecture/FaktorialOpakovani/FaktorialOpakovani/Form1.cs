﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FaktorialOpakovani
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            int n = Convert.ToInt32(textBoxVstup.Text);
            if (n < 0)
            {
                MessageBox.Show("Zadejte kladné číslo", "Chyba");
                return;
            }

            progressBar1.Maximum = n;
            BigInteger vysledek = new BigInteger();
            vysledek = 1;            
            
            for (int i = n; i >= 2; i--)
            {
                vysledek = BigInteger.Multiply(i, vysledek);
                progressBar1.Value = i;                
            }
            richTextBoxVystup.Text = Convert.ToString(vysledek);
        }
                
    }
}
