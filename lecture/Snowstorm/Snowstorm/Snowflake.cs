﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snowstorm
{
    class Snowflake
    {
        private float x, y;

        public Snowflake(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public void Draw(Graphics kp)
        {
            Font f = new Font("Arial", 20);
            kp.DrawString("*", f, Brushes.White, x, y);
        }


    }
}
