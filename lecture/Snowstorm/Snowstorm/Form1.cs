﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snowstorm
{
    public partial class Form1 : Form
    {
        Snowflake sf;
        List<Snowflake> snowflakes = new List<Snowflake>();

        public Form1()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;

            foreach (Snowflake item in snowflakes)
            {
                item.Draw(kp);

            }            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Random rnd = new Random();
            snowflakes.Add(new Snowflake(rnd.Next(panel1.Width), 0));


            Refresh();
        }
    }
}