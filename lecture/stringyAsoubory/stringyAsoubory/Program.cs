﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stringyAsoubory
{
    class Program
    {
        static void Main(string[] args)
        {
            //PraceSeStringem();
            //PraceSeStringBuilderem();
            //Formatovani();
            //Cviceni1();
            Cviceni2();

            //StreamReader file = new StreamReader("text.txt");

            //string radek = file.ReadLine();
            //while (radek != null)
            //{
            //    Console.WriteLine(radek);
            //    radek = file.ReadLine();
            //}
            //file.Close();

            Console.ReadKey();
        }
        static void PraceSeStringem()
        {
            //stringy
            string text = "Hello world";

            text = "Hello" + " " + "World";

            char prvniZnak = text[0];
            char posledniZnak = text[text.Length - 1];

            Console.WriteLine(text);
            Console.WriteLine(prvniZnak);
            Console.WriteLine(posledniZnak);

            char znak = 'l';
            //text[0] = 'k'; - nefunguje --> slouží ke čtení, ne k modifikaci

            foreach (char item in text)
            {
                Console.WriteLine(item);
            }

            if (text[0] == 'H')
            {
                Console.WriteLine("Znak je H");
            }

            string text2 = null;
            if (string.IsNullOrEmpty(text2))
            {
                Console.WriteLine("Text2 je prazdny");
            }
            
            string text3 = "ahoj";
            if (text3.Equals("ahoj"))
            {
                Console.WriteLine("text3 je ahoj");
            }

            Console.WriteLine(2 == 2.0);
            Console.WriteLine(2.Equals(2.0));

            string slovaScarkou = "ahoj,cau,dobry,den,ahoj";
            string[] poleStringu = slovaScarkou.Split(',');
            foreach (string item in poleStringu)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("trim");
            string bezmezer = "Hello world";
            string smezerami = " Hello world ";
            Console.WriteLine(bezmezer);
            Console.WriteLine(smezerami);
            Console.WriteLine(smezerami.Trim());

            Console.WriteLine("Ahoj \njak se mas?");
            Console.WriteLine("Ahoj \tjak se mas?");

            Console.WriteLine(bezmezer.ToUpper());
            Console.WriteLine(bezmezer.ToLower());

            Console.WriteLine(bezmezer.Substring(3));
            Console.WriteLine(bezmezer.Substring(3, 5));
        }
        static void PraceSeStringBuilderem()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Ahoj");
            sb.Append(" cau");
            sb.Append(" cau");
            sb.AppendLine();
            sb.Append("dobry den");

            sb.ToString();

            Console.WriteLine(sb);
        }
        static void Formatovani()
        {
            double pi = Math.PI;

            string text1 = "Hodnota pi je " + pi;            
            string text2 = $"Hodnota pi na 2 desetinna mista je {pi:f2}";
            string text3 = string.Format("Hodnota pi na 5 desetinnych mist je {0:f5} dalsi parametr je {1}", pi, 5);

            Console.WriteLine(text1);
            Console.WriteLine(text2);
            Console.WriteLine(text3);

            DateTime today = DateTime.Now;

            StringBuilder sb = new StringBuilder();
            sb.Append("Dnes je ");
            sb.Append($"{today}");
            sb.AppendLine();
            sb.Append($"{today:F}");
            sb.Append(today.ToString("F"));
            sb.Append("Today is ");
            sb.AppendLine();
            sb.Append(today.ToString("F", System.Globalization.CultureInfo.GetCultureInfo("en-US")));
            sb.AppendLine();
            sb.Append(today.ToString("d MM", System.Globalization.CultureInfo.GetCultureInfo("en-US")));

            Console.WriteLine(sb);
        }
        //Vytvorit retezec
        //Spocitat vyskyty nejakeho znaku
        static void Cviceni1()
        {
            char znak = 'a';
            string text = "asdhkskjlafasjkldfnjklasdbhlsadfjklasdfkjlasdfk jasdf asdfjkh asdklf asdjkl kasdf asfkl f asdf klasdklflasdf";
            int counter = 0;
            foreach (char item in text)
            {
                if (item == znak)
                    counter++;                
            }
            Console.WriteLine("Znak '{0}' se vyskytuje v textu {1} krat", znak, counter);
        }
        static void Cviceni2()
        {
            Random rnd = new Random();

            rnd.Next(0, 100);

            //char znak = (char)65;
            //int asciiKod = (int)'l';

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < 100; i++)
            {
                
                int cislo = rnd.Next(65, 97);
                char znak = (char)cislo;
                sb.Append(znak);
            }

            Console.WriteLine(sb);
            //Console.WriteLine(znak);
            //Console.WriteLine(asciiKod);
        }
    }
}
