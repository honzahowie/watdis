﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.CheckedListBox;

namespace WindowsForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void labelName_Click(object sender, EventArgs e)
        {
            textBoxName.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Uživatel zmáčkl tlačítko";
            //validace vstupního textu
            if (textBoxName.Text == "")
            {
                MessageBox.Show("Zadejte jméno", "Chybné zadání", MessageBoxButtons.OK);
                return;
            }
            DialogResult result = MessageBox.Show("Ahoj " + textBoxName.Text + ", chceš pokračovat?", "Ahoj", MessageBoxButtons.YesNo);
            MessageBox.Show("Uživatel vybral: " + (result == DialogResult.Yes? "Ano" : "Ne"));
            //MessageBox.Show("Uživatel vybral: " + result.ToString());


            toolStripStatusLabel1.Text = "Aplikace běží";
        }

        private void buttonOther_Click(object sender, EventArgs e)
        {
            CheckedItemCollection checkedItems = checkedListBox1.CheckedItems;
            foreach (object item in checkedItems)
            {
                MessageBox.Show("Vybrané pole: " + Convert.ToString(item));
            }
        }

        private void checkBoxVse_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                checkedListBox1.SetItemChecked(i, true);
            }
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            MessageBox.Show("Vybrané datum: Start=" + e.Start.ToShortDateString() + " End=" + e.End.ToShortDateString());
        }

        private void zavřítToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void barvaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            Color color = colorDialog1.Color;
            MessageBox.Show(color.ToString());
        }

        private void otevřítToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                MessageBox.Show(openFileDialog1.FileName);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value >= 99) 
            {
                progressBar1.Value = 0;
            }
            progressBar1.Value += 1;
        }
    }
}
