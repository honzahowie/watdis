﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Razeni2
{
    class Program
    {
        static Random rnd = new Random();

        static void Main(string[] args)
        {
            int[] a = new int[10];
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = rnd.Next(a.Length);
            }
            InsertionSort(a);
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i] + " ");
            }
            Console.ReadKey();
        }

        static void InsertionSort(int[] a)
        {
            for (int i = 0; i < a.Length-1; i++)
            {
                int j = i + 1;
                int p = a[j];
                while (j > 0 && p < a[j-1])
                {
                    a[j] = a[j - 1];
                    j--;
                }
                a[j] = p;
            }
        }

        static void SelectionSort (int[] a)
        {
            for (int j = 0; j < a.Length-1; j++)
            {
                int min = j;
                for (int i = j+1; i < a.Length; i++)
                {
                    if (a[i] < a[min])
                    {
                        min = i;
                    }
                    Prohozeni(a, j, min);
                }
            }
        }

        static void Prohozeni(int[] a, int i1, int i2)
        {
            if (i1 != i2)
            {
                int p = a[i1];
                a[i1] = a[i2];
                a[i2] = p;
            }
        }
    }
}
