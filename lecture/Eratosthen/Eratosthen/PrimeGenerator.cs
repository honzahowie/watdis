﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eratosthen
{
    class PrimeGenerator : IPrimeGenerator
    {
        private int lowerBound;
        private int upperBound;

        public PrimeGenerator(int start, int end)
        {
            lowerBound = start;
            upperBound = end;
        }

        public List<int> PrimesFromIntervalOldWay()
        {
            List<int> primes = new List<int>();

            for (int number = lowerBound; number <= upperBound; number++)
            {
                if (IsPrime(number))
                    primes.Add(number);
            }


            return primes;
            
        }

        public List<int> PrimesFromIntervalEratosthen()
        {
            //inicializace
            bool[] sieve = new bool[upperBound + 1];
            for (int i = 2; i <= upperBound; i++)
            {
                sieve[i] = true;
            }

            //eratosthen
            for (int i = 2; i < Math.Sqrt(upperBound); i++)
            {
                if (sieve[i])
                {
                    for (int j = i*2; j < upperBound; j += i)
                    {
                        sieve[j] = false;
                    }
                }
            }

            //result
            List<int> primes = new List<int>();
            for (int number = lowerBound; number < upperBound; number++)
            {
                if (sieve[number]) primes.Add(number);
            }

            return primes;
        }

        private bool IsPrime(int number)
        {
            for (int i = 2; i <= Math.Sqrt(number); i++)
            {
                if (number % i == 0)
                    return false;
            }

            return true;
        }
    }
}
