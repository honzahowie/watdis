﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eratosthen
{
    class Program
    {
        static void Main(string[] args)
        {
            int start = ReadNumber("Zadejte dolní mez");
            int end = ReadNumber("Zadejte horní mez");

            PrimeGenerator pg = new PrimeGenerator(start, end);
            List<int> primes = pg.PrimesFromIntervalOldWay();
            Console.WriteLine("Prvočísel je " + primes.Count());
            Console.WriteLine(String.Join(" ", primes));

            List<int> primesErat = pg.PrimesFromIntervalEratosthen();
            Console.WriteLine("Prvočísel je " + primesErat.Count());
            Console.WriteLine(String.Join(" ", primesErat));


            Console.ReadKey();
        }

        private static int ReadNumber(string text)
        {
            int number;
            Console.WriteLine(text);
            while (!int.TryParse(Console.ReadLine(), out number))
                Console.WriteLine("Zadejte validní číslo");


            return number;
        }

        
    }
}
