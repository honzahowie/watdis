﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roman1
{
    interface IBinaryConverter
    {
        string DecimalToBinaryEasy(int number);

        string DecimalToBinary(int number);

        int BinaryToDecimalEasy(string binaryNumber);

        int BinaryToDecimal(string binaryNumber);
    }
}
