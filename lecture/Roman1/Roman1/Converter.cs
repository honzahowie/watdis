﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roman1
{
    class Converter : IRomanConverter, IBinaryConverter, IHexConverter
    {
        public int ToArab(string roman)
        {
            //LV na 55
            int number = 0;
           
            for (int i = 0; i < roman.Length; i++)
            {                
                number += RomanToNumber[roman[i]];
            }

            return number;
        }

        public string ToRoman(int number)
        {
            //55 na LV
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<int, string> item in NumberToRoman)
            {
                while (item.Key <= number)
                {
                    sb.Append(item.Value);
                    number -= item.Key;
                }
            }


            return sb.ToString();
        }

        public string DecimalToBinaryEasy(int number)
        {
            return Convert.ToString(number, 2);
        }

        public string DecimalToBinary(int number)
        {
            List<int> binaryNumber = new List<int>();

            while (number > 0)
            {
                binaryNumber.Add(number % 2);
                number = number / 2;
            }

            binaryNumber.Reverse();

            return String.Join("", binaryNumber);
        }

        public int BinaryToDecimalEasy(string binaryNumber)
        {
            return Convert.ToInt32(binaryNumber, 2);
        }

        public int BinaryToDecimal(string binaryNumber)
        {
            char[] binaryArr = binaryNumber.ToArray();
            Array.Reverse(binaryArr);

            int number = 0;
            for (int i = 0; i < binaryArr.Length; i++)
            {
                if (binaryArr[i] == '1')
                {
                    number += (int)Math.Pow(2, i);
                }
            }
            return number;
        }

        public string DecimalToHex(int number)
        {
            throw new NotImplementedException();
        }

        public int HexToDecimalEasy(string hexNumber)
        {
            return Convert.ToInt32(hexNumber, 16);
        }

        private Dictionary<int, string> NumberToRoman = new Dictionary<int, string>
        {
            {1000, "M"}, {900, "CM"}, {500, "D"}, {400, "CD"}, {100, "C"}, {90, "XC"}, {50, "L"}, {40, "XL"}, {10, "X"}, {9, "IX"}, {5, "V"}, {4, "IV"}, {1, "I"}
        };

        private Dictionary<char, int> RomanToNumber = new Dictionary<char, int>
        {
            {'M', 1000}, {'D', 500}, {'C', 100}, {'L', 50}, {'X', 10}, {'V', 5}, {'I', 1}
        };
    }   
}
