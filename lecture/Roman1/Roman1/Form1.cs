﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Roman1
{
    public partial class Form1 : Form
    {
        private Converter converter = new Converter();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonToRoman_Click(object sender, EventArgs e)
        {
            int number = Convert.ToInt32(textBoxInput.Text);
            string romanNumber = converter.ToRoman(number);
            labelOutput.Text = romanNumber;
        }

        private void buttonToBinary_Click(object sender, EventArgs e)
        {
            int number = Convert.ToInt32(textBoxBinDecInput.Text);

            textBoxBinDecOutput.Text = converter.DecimalToBinary(number);
        }

        private void buttonToDecimal_Click(object sender, EventArgs e)
        {
            string binaryNumber = textBoxBinDecInput.Text;
            int number = converter.BinaryToDecimal(binaryNumber);

            textBoxBinDecOutput.Text = Convert.ToString(number);
        }

        private void buttonHexToDec_Click(object sender, EventArgs e)
        {
            string hexNumber = textBoxHexDecInput.Text;
            int number = converter.HexToDecimalEasy(hexNumber);

            textBoxHexDecOutput.Text = Convert.ToString(number);
        }
    }
}
