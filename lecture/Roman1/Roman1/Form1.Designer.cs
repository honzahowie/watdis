﻿namespace Roman1
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonToRoman = new System.Windows.Forms.Button();
            this.buttonToArab = new System.Windows.Forms.Button();
            this.textBoxInput = new System.Windows.Forms.TextBox();
            this.labelOutput = new System.Windows.Forms.Label();
            this.textBoxBinDecInput = new System.Windows.Forms.TextBox();
            this.buttonToBinary = new System.Windows.Forms.Button();
            this.buttonToDecimal = new System.Windows.Forms.Button();
            this.textBoxBinDecOutput = new System.Windows.Forms.TextBox();
            this.textBoxHexDecInput = new System.Windows.Forms.TextBox();
            this.buttonToHex = new System.Windows.Forms.Button();
            this.buttonHexToDec = new System.Windows.Forms.Button();
            this.textBoxHexDecOutput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonToRoman
            // 
            this.buttonToRoman.Location = new System.Drawing.Point(16, 47);
            this.buttonToRoman.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonToRoman.Name = "buttonToRoman";
            this.buttonToRoman.Size = new System.Drawing.Size(100, 28);
            this.buttonToRoman.TabIndex = 0;
            this.buttonToRoman.Text = "Na římské";
            this.buttonToRoman.UseVisualStyleBackColor = true;
            this.buttonToRoman.Click += new System.EventHandler(this.buttonToRoman_Click);
            // 
            // buttonToArab
            // 
            this.buttonToArab.Location = new System.Drawing.Point(152, 47);
            this.buttonToArab.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonToArab.Name = "buttonToArab";
            this.buttonToArab.Size = new System.Drawing.Size(100, 28);
            this.buttonToArab.TabIndex = 1;
            this.buttonToArab.Text = "Na arabské";
            this.buttonToArab.UseVisualStyleBackColor = true;
            // 
            // textBoxInput
            // 
            this.textBoxInput.Location = new System.Drawing.Point(16, 15);
            this.textBoxInput.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxInput.Name = "textBoxInput";
            this.textBoxInput.Size = new System.Drawing.Size(235, 22);
            this.textBoxInput.TabIndex = 2;
            // 
            // labelOutput
            // 
            this.labelOutput.AutoSize = true;
            this.labelOutput.Location = new System.Drawing.Point(17, 84);
            this.labelOutput.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelOutput.Name = "labelOutput";
            this.labelOutput.Size = new System.Drawing.Size(0, 17);
            this.labelOutput.TabIndex = 3;
            // 
            // textBoxBinDecInput
            // 
            this.textBoxBinDecInput.Location = new System.Drawing.Point(607, 14);
            this.textBoxBinDecInput.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxBinDecInput.Name = "textBoxBinDecInput";
            this.textBoxBinDecInput.Size = new System.Drawing.Size(287, 22);
            this.textBoxBinDecInput.TabIndex = 4;
            // 
            // buttonToBinary
            // 
            this.buttonToBinary.Location = new System.Drawing.Point(607, 46);
            this.buttonToBinary.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonToBinary.Name = "buttonToBinary";
            this.buttonToBinary.Size = new System.Drawing.Size(129, 28);
            this.buttonToBinary.TabIndex = 5;
            this.buttonToBinary.Text = "Do binárních";
            this.buttonToBinary.UseVisualStyleBackColor = true;
            this.buttonToBinary.Click += new System.EventHandler(this.buttonToBinary_Click);
            // 
            // buttonToDecimal
            // 
            this.buttonToDecimal.Location = new System.Drawing.Point(761, 47);
            this.buttonToDecimal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonToDecimal.Name = "buttonToDecimal";
            this.buttonToDecimal.Size = new System.Drawing.Size(133, 28);
            this.buttonToDecimal.TabIndex = 6;
            this.buttonToDecimal.Text = "Do decimálních";
            this.buttonToDecimal.UseVisualStyleBackColor = true;
            this.buttonToDecimal.Click += new System.EventHandler(this.buttonToDecimal_Click);
            // 
            // textBoxBinDecOutput
            // 
            this.textBoxBinDecOutput.Location = new System.Drawing.Point(607, 84);
            this.textBoxBinDecOutput.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textBoxBinDecOutput.Name = "textBoxBinDecOutput";
            this.textBoxBinDecOutput.Size = new System.Drawing.Size(287, 22);
            this.textBoxBinDecOutput.TabIndex = 7;
            // 
            // textBoxHexDecInput
            // 
            this.textBoxHexDecInput.Location = new System.Drawing.Point(607, 164);
            this.textBoxHexDecInput.Name = "textBoxHexDecInput";
            this.textBoxHexDecInput.Size = new System.Drawing.Size(287, 22);
            this.textBoxHexDecInput.TabIndex = 8;
            // 
            // buttonToHex
            // 
            this.buttonToHex.Location = new System.Drawing.Point(607, 202);
            this.buttonToHex.Name = "buttonToHex";
            this.buttonToHex.Size = new System.Drawing.Size(129, 28);
            this.buttonToHex.TabIndex = 9;
            this.buttonToHex.Text = "Do hexadec.";
            this.buttonToHex.UseVisualStyleBackColor = true;
            // 
            // buttonHexToDec
            // 
            this.buttonHexToDec.Location = new System.Drawing.Point(761, 202);
            this.buttonHexToDec.Margin = new System.Windows.Forms.Padding(4);
            this.buttonHexToDec.Name = "buttonHexToDec";
            this.buttonHexToDec.Size = new System.Drawing.Size(133, 28);
            this.buttonHexToDec.TabIndex = 10;
            this.buttonHexToDec.Text = "Do decimálních";
            this.buttonHexToDec.UseVisualStyleBackColor = true;
            this.buttonHexToDec.Click += new System.EventHandler(this.buttonHexToDec_Click);
            // 
            // textBoxHexDecOutput
            // 
            this.textBoxHexDecOutput.Location = new System.Drawing.Point(607, 248);
            this.textBoxHexDecOutput.Name = "textBoxHexDecOutput";
            this.textBoxHexDecOutput.Size = new System.Drawing.Size(287, 22);
            this.textBoxHexDecOutput.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.textBoxHexDecOutput);
            this.Controls.Add(this.buttonHexToDec);
            this.Controls.Add(this.buttonToHex);
            this.Controls.Add(this.textBoxHexDecInput);
            this.Controls.Add(this.textBoxBinDecOutput);
            this.Controls.Add(this.buttonToDecimal);
            this.Controls.Add(this.buttonToBinary);
            this.Controls.Add(this.textBoxBinDecInput);
            this.Controls.Add(this.labelOutput);
            this.Controls.Add(this.textBoxInput);
            this.Controls.Add(this.buttonToArab);
            this.Controls.Add(this.buttonToRoman);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonToRoman;
        private System.Windows.Forms.Button buttonToArab;
        private System.Windows.Forms.TextBox textBoxInput;
        private System.Windows.Forms.Label labelOutput;
        private System.Windows.Forms.TextBox textBoxBinDecInput;
        private System.Windows.Forms.Button buttonToBinary;
        private System.Windows.Forms.Button buttonToDecimal;
        private System.Windows.Forms.TextBox textBoxBinDecOutput;
        private System.Windows.Forms.TextBox textBoxHexDecInput;
        private System.Windows.Forms.Button buttonToHex;
        private System.Windows.Forms.Button buttonHexToDec;
        private System.Windows.Forms.TextBox textBoxHexDecOutput;
    }
}

