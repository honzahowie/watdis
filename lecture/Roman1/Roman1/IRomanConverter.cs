﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roman1
{
    interface IRomanConverter
    {
        string ToRoman(int number);
        int ToArab(string roman);
    }
}
