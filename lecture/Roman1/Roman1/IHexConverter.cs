﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roman1
{
    interface IHexConverter
    {
        string DecimalToHex(int number);

        int HexToDecimalEasy(string hexNumber);
    }
}
