﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MergeSort
{
    class MergeSort
    {
        public void Sort(int[] arr, int start, int end)
        {
            if (end > start)
            {            
            //najdi middle
            int middle = (start + end) / 2;
            
            //zavolat sort pro obě půlky
            Sort(arr, start, middle); //sort left
            Sort(arr, middle + 1, end); //sort right

            //mergenout ty dvě seřazená pole,
            Merge(arr, start, middle, end);
            }
        }

        public void Merge(int[] arr, int start, int middle, int end)
        {
            int nL = middle - start + 1;
            int nR = end - middle;

            int[] tempL = new int[nL];
            int[] tempR = new int[nR];

            for (int i = 0; i < nL; i++)
            {
                tempL[i] = arr[start + i];
            }
            for (int i = 0; i < nR; i++)
            {
                tempR[i] = arr[middle + i + 1];
            }

            int iL = 0;
            int iR = 0;
            int iArr = start;
            while (iL < nL && iR < nR)
            {
                if (tempL[iL] < tempR[iR])
                {
                    arr[iArr] = tempL[iL];
                    iL = iL + 1;
                    iArr = iArr + 1;
                }
                else
                {
                    arr[iArr] = tempR[iR];
                    iR++;
                    iArr++;
                }
            }
            while (iL < nL)
            {
                arr[iArr] = tempL[iL];
                iL++;
                iArr++;
            }
            while (iR < nR)
            {
                arr[iArr] = tempR[iR];
                iR++;
                iArr++;
            }
        }
    }
}
