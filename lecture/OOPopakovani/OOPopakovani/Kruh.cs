﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPopakovani
{
    class Kruh : ITvar
    {
        private int r;

        public Kruh(int r)
        {
            this.r = r;
        }

        public string VratInformace()
        {
            return "Ahoj, já jsem kruh a mám poloměr " + r; 
        }

        public decimal VypoctiObsah()
        {
            double obsah = Math.PI * r * r;
            return Convert.ToDecimal(obsah);
        }
    }
}
