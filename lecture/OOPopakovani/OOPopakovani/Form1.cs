﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOPopakovani
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            List<ITvar> list = new List<ITvar>();

            for (int i = 0; i < 20; i++)
            {                
                int tvarIndex = rnd.Next(0, 3);
                ITvar tvar = null;
                switch (tvarIndex)
                {
                    case 0:
                        tvar = new Kruh(rnd.Next(1,50));
                        break;
                    case 1:
                        tvar = new Obdelnik(rnd.Next(1, 50), rnd.Next(1, 50));
                        break;
                    case 2:
                        tvar = new Ctverec(rnd.Next(1, 50));
                        break;
                    default:
                        break;
                }
                list.Add(tvar);
            }
            String vysledek = "";
            foreach (ITvar kys in list)
            {
                vysledek += kys.VratInformace();
                vysledek += Environment.NewLine;
            }
            richTextBox1.Text = vysledek;
        }
    }
}
