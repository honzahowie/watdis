﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPopakovani
{
    class Obdelnik : ITvar
    {
        protected int a;
        protected int b;

        public Obdelnik(int a, int b)
        {
            this.a = a;
            this.b = b;
        }

        public virtual string VratInformace()
        {
            return "Ahoj, já jsem obdélník a mám strany " + a + " " + b;
        }

        public decimal VypoctiObsah()
        {
            int obsah = a * b;
            return Convert.ToDecimal(obsah);
        }
    }
}
