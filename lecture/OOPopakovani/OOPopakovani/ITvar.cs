﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPopakovani
{
    interface ITvar
    {
        decimal VypoctiObsah();
        string VratInformace();
    }
}
