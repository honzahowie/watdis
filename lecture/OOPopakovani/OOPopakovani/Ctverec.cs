﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPopakovani
{
    class Ctverec : Obdelnik
    {
        public Ctverec(int a) : base(a, a)
        {
            
        }

        public override string VratInformace()
        {
            return "Ahoj, já jsem čtverec a mám stranu " + a;
        }
    }
}
