﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrafikaOpakovani
{
    public partial class Form1 : Form
    {
        List<Vektor> list = new List<Vektor>();
        
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            StreamReader txt = new StreamReader("vektorovy-obrazek.csv");
            txt.ReadLine();
            string radek = txt.ReadLine();

            while (radek != null)
            {
                //kod
                string[] pole = radek.Split(';');
                Vektor v1 = new Vektor(pole);
                list.Add(v1);
                
                
                
                radek = txt.ReadLine();
            }

        }
    }
}
