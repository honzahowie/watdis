﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrafikaOpakovani
{
    class Vektor
    {
        public int Xstart;
        public int Xend;
        public int Ystart;
        public int Yend;

        public Vektor(string[] pole)
        {
            Xstart = Convert.ToInt32(pole[0]);
            Xend = Convert.ToInt32(pole[1]);
            Ystart = Convert.ToInt32(pole[2]);
            Yend = Convert.ToInt32(pole[3]);
        }
    }
}
