b)
časová složitost
	jak dlouho trcá algoritmu seřadit čísla
	druhy: kvadratický (bubble sort), lineárně logaritmický (merge sort)
stabilita
	stabilní - neprohazuje prvky se stejnou hodnotou (insertion sort)
	nestabilní - prohazuje prvky se stejnou hodnotou (selection sort)
přirozenost
	přirozený algoritmus již částečně seřazenou posloupnost znovu neprochází a neseřazuje (merge sort)
	opak - quicksort

c)
algoritmus nějdříve celou řadu čísel rozdělí na menší "problémy" (skupiny čísel), které je potom následně schopen seřadit a seřazené skupiny poté spojí do finální seřazené posloupnost
např. merge sort

d)
CLI - Command Line Interface = rozhraní s příkazovým řádkem
GUI - Graphical User Interface = rozhraní "běžného programu", grafické ovládací prvky

e)
textBox - přijímá vstupní text a může i zobrazovat výstup
button - ovládací tlačítko, spouští nějakou akci
label - popisek ovládacích prvků
timer - časovač (využíváme např. pro animace objektů)
checkBox - zaškrtávací poličko (hranatý)
radioButton - přepínač (kulatý)
form - okno formuláře
panel - pro kreslení grafických objektů
menuStrip - záhlaví formuláře
pictureBox - pro vkládání obrázku

f)
jedničková - počítání na prstech, čárky v hospodě - dobrá pro sčítání počtu jednotlivých prvků
desítková - nejpoužívanější v běžném životě
dvojková - počítače fungují v dvojkové soustavě
šestnáctková - dá se využít pro zjednodušení velmi dlouhých binárních čísel

g)
seznam prvků (např. písmena V,X,L... u římských čísel), které jsou přiřazeny ke konkrétním hodnotám (V = 5, X = 10, L = 50...)
používá se většinou při převodu do jiné číselné soustavy, která využívá písmena (římská čísla, šestnáctková soustava)

h)
rozhraní třídy, určuje základní zobrazení, ale přímo v interface nic neprogramujeme, pouze ho implementujeme do konkrétních tříd
dědičnost zajistíme pomocí znaku ":" v hlavičce třídy 