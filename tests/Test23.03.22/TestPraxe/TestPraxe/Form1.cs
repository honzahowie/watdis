﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestPraxe
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSUM_Click(object sender, EventArgs e)
        {
            //double n = Convert.ToDouble(textBoxN.Text);
            //double x = Convert.ToDouble(textBoxX.Text);
            //double result;
            //for (int i = 1; i < n; i++)
            //{
            //    result = Math.Pow(n, x);
            //    return;
            //}
            //labelOutput.Text = Convert.ToString(result);
        }

        private void radioButtonBinary_CheckedChanged(object sender, EventArgs e)
        {
            if (textBoxInput.Text == "")
            {
                MessageBox.Show("Zadejte číslo", "Chybné zadání", MessageBoxButtons.OK);
                return;
            }           
            
            int input = Convert.ToInt32(textBoxInput.Text);
            textBoxOutput.Text = Convert.ToString(input, 2);
        }        

        private void radioButtonHexadec_CheckedChanged(object sender, EventArgs e)
        {
            if (textBoxInput.Text == "")
            {
                MessageBox.Show("Zadejte číslo", "Chybné zadání", MessageBoxButtons.OK);
                return;
            }

            int input = Convert.ToInt32(textBoxInput.Text);
            textBoxOutput.Text = Convert.ToString(input, 16);
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void ahojToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new Elipsa();
            form.Show();
        }
    }
}
