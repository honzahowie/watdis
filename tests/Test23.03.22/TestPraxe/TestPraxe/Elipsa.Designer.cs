﻿namespace TestPraxe
{
    partial class Elipsa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxSirka = new System.Windows.Forms.TextBox();
            this.textBoxVyska = new System.Windows.Forms.TextBox();
            this.buttonVykresli = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonObsah = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(159, 11);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(636, 434);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint_1);
            // 
            // textBoxSirka
            // 
            this.textBoxSirka.Location = new System.Drawing.Point(13, 13);
            this.textBoxSirka.Name = "textBoxSirka";
            this.textBoxSirka.Size = new System.Drawing.Size(100, 20);
            this.textBoxSirka.TabIndex = 1;
            // 
            // textBoxVyska
            // 
            this.textBoxVyska.Location = new System.Drawing.Point(13, 40);
            this.textBoxVyska.Name = "textBoxVyska";
            this.textBoxVyska.Size = new System.Drawing.Size(100, 20);
            this.textBoxVyska.TabIndex = 2;
            // 
            // buttonVykresli
            // 
            this.buttonVykresli.Location = new System.Drawing.Point(13, 67);
            this.buttonVykresli.Name = "buttonVykresli";
            this.buttonVykresli.Size = new System.Drawing.Size(75, 23);
            this.buttonVykresli.TabIndex = 3;
            this.buttonVykresli.Text = "Vykresli";
            this.buttonVykresli.UseVisualStyleBackColor = true;
            this.buttonVykresli.Click += new System.EventHandler(this.buttonVykresli_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(120, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Šířka";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(119, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Výška";
            // 
            // buttonObsah
            // 
            this.buttonObsah.Location = new System.Drawing.Point(13, 119);
            this.buttonObsah.Name = "buttonObsah";
            this.buttonObsah.Size = new System.Drawing.Size(75, 23);
            this.buttonObsah.TabIndex = 6;
            this.buttonObsah.Text = "Obsah elipsy";
            this.buttonObsah.UseVisualStyleBackColor = true;
            this.buttonObsah.Click += new System.EventHandler(this.buttonObsah_Click);
            // 
            // Elipsa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonObsah);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonVykresli);
            this.Controls.Add(this.textBoxVyska);
            this.Controls.Add(this.textBoxSirka);
            this.Controls.Add(this.panel1);
            this.Name = "Elipsa";
            this.Text = "Elipsa";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxSirka;
        private System.Windows.Forms.TextBox textBoxVyska;
        private System.Windows.Forms.Button buttonVykresli;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonObsah;
    }
}