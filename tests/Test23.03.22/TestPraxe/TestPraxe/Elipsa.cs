﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestPraxe
{
    public partial class Elipsa : Form
    {
        float sirka = 0;
        float vyska = 0;
        public Elipsa()
        {
            InitializeComponent();
        }

        private void buttonVykresli_Click(object sender, EventArgs e)
        {
            sirka = Convert.ToSingle(textBoxSirka.Text);
            vyska = Convert.ToSingle(textBoxVyska.Text);

            panel1.Refresh();
        }       

        private void panel1_Paint_1(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Pen p = new Pen(Color.Blue, 2);
            g.DrawEllipse(p, 50, 50, sirka, vyska);
        }

        private void buttonObsah_Click(object sender, EventArgs e)
        {
            double obsah = Convert.ToDouble(3.1415926535897931) * (Convert.ToSingle(textBoxSirka.Text)/2) * (Convert.ToSingle(textBoxVyska.Text)/2);
            MessageBox.Show(Convert.ToString(obsah), "Obsah elipsy");
        }
    }    
}
