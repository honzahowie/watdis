﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortovaniTest
{
    class Program
    {
        static Random rnd = new Random();

        static void Main(string[] args)
        {
            int[] pole = new int[10000];
            for (int i = 0; i < pole.Length; i++)
            {
                pole[i] = rnd.Next(0, 21);
            }

            Bubble(pole);

            for (int i = 0; i < pole.Length; i++)
            {
                Console.Write(pole[i] + " ");
            }
            Console.ReadKey();
        }
        static void Bubble(int[] pole) // Bubble sort porovnává hodnoty čísel dvou sousedících indexů. Pokud je hodnota následujícího pole menší, než je hodnota současného, dojde k jejich prohození. Postupně tak nejmenší (nebo největší, podle nastavení >/< v if) hodnota "probublá" na konec posloupnosti a je seřazena. V následující vlně se sortování ukončí u již seřazeného pole.
        {
            for (int i = 0; i < pole.Length; i++) // cyklus, který postupně prochází celým polem a ukládá již seřazená čísla
            {
                for (int j = 0; j < pole.Length - 1 - i; j++) // vnořený cyklus, který porovnává hodnoty čísel dvou sousedících indexů a vynechává již seřazená čísla
                {
                    if (pole[j+1] < pole[j]) // pokud je hodnota čísla na dalším indexu (j+1) menší, než na současném (j), dojde k prohození čísel
                    {
                        Prohozeni(pole, j, j + 1);
                    }
                }
            }
        }
        static void Prohozeni(int[] pole, int j, int j1)
        {
            int p = pole[j];
            pole[j] = pole[j1];
            pole[j1] = p;
        }
    }
}
