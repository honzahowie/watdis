﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Zadej číslo: ");
            int cislo = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Rekurze: " + FaktorialRek(cislo));

            int factCyklus = 1;
            for (int i = 1; i <= cislo; i++)
            {
                factCyklus = factCyklus * i;           
            }
            Console.WriteLine("Cyklus: " + factCyklus);

            StreamWriter output = new StreamWriter("faktorialCyklus.txt");
            output.Write(factCyklus);
            output.Close();
            Console.WriteLine("Výsledek uložen do souboru 'faktorialCyklus.txt'");


            Console.ReadKey();
        }
        static int FaktorialRek(int cislo)
        {
            int i = 1;
            int factRekurze = 1;

            while (i <= cislo)
            {
                factRekurze = factRekurze * i;
                i++;
            }
            return factRekurze;
        }
    }
}
