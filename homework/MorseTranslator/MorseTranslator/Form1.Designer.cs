﻿
namespace MorseTranslator
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxInput = new System.Windows.Forms.TextBox();
            this.buttonToMorse = new System.Windows.Forms.Button();
            this.buttonToText = new System.Windows.Forms.Button();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // textBoxInput
            // 
            this.textBoxInput.Location = new System.Drawing.Point(24, 13);
            this.textBoxInput.Name = "textBoxInput";
            this.textBoxInput.Size = new System.Drawing.Size(239, 22);
            this.textBoxInput.TabIndex = 0;
            // 
            // buttonToMorse
            // 
            this.buttonToMorse.Location = new System.Drawing.Point(24, 42);
            this.buttonToMorse.Name = "buttonToMorse";
            this.buttonToMorse.Size = new System.Drawing.Size(113, 23);
            this.buttonToMorse.TabIndex = 1;
            this.buttonToMorse.Text = "Do morseovky";
            this.buttonToMorse.UseVisualStyleBackColor = true;
            this.buttonToMorse.Click += new System.EventHandler(this.buttonToMorse_Click);
            // 
            // buttonToText
            // 
            this.buttonToText.Location = new System.Drawing.Point(187, 41);
            this.buttonToText.Name = "buttonToText";
            this.buttonToText.Size = new System.Drawing.Size(75, 23);
            this.buttonToText.TabIndex = 2;
            this.buttonToText.Text = "Na text";
            this.buttonToText.UseVisualStyleBackColor = true;
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Location = new System.Drawing.Point(24, 71);
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.Size = new System.Drawing.Size(239, 22);
            this.textBoxOutput.TabIndex = 3;
            this.textBoxOutput.TextChanged += new System.EventHandler(this.textBoxOutput_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.textBoxOutput);
            this.Controls.Add(this.buttonToText);
            this.Controls.Add(this.buttonToMorse);
            this.Controls.Add(this.textBoxInput);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxInput;
        private System.Windows.Forms.Button buttonToMorse;
        private System.Windows.Forms.Button buttonToText;
        private System.Windows.Forms.TextBox textBoxOutput;
    }
}

