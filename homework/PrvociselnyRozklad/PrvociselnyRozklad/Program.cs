﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrvociselnyRozklad
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 0;
            string result = null;

            Console.Write("Zadejte číslo : ");
            n = Convert.ToInt32(Console.ReadLine());

            while (n != 1)
            {
                int x = 0;
                for (int i = 2; i <= n; i++)
                {
                    if ((n % i) == 0)
                    {
                        x = i;
                        break;
                    }
                }

                result += x.ToString() + " * ";


                n = n / x;
            }

            Console.Write("Rozklad na prvočinitele: {0}1", result);
            Console.ReadKey();
        }
    }
}
