﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinomickaVeta
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int factN = 1;
            for (int i = 1; i <= n ; i++)
            {
                factN = factN * i;
            }
            Console.WriteLine(factN);
            //int cN = factN / (k! * (n - k)!);


            Console.ReadKey();
        }
    }
}
