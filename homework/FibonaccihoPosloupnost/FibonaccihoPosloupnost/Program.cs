﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace FibonaccihoPosloupnost
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Zadej poslední člen posloupnosti:");
            int n = Convert.ToInt32(Console.ReadLine());
            BigInteger x = 0;
            BigInteger y = 1;
            Console.WriteLine("0: {0:N0}", x);
            Console.WriteLine("1: {0:N0}", y);
            for (int i = 2; i <= n; i++)
            {
                BigInteger z = x + y;
                Console.WriteLine("{0}: {1:N0}", i, z);
                x = y;
                y = z;
            }

            Fibonacci_Recursive(n);


            Console.ReadKey();
        }
        static void Fibonacci_Recursive(int n)
        {            
            Console.WriteLine("Rekurzivně");
            Fibonacci_Rec_Temp(0, 1, 1, n);
        }
        static void Fibonacci_Rec_Temp(int x, int y, int i, int n)
        {
            if (i <= n)
            {
                Console.Write("{0} ", x);
                Fibonacci_Rec_Temp(y, x + y, i + 1, n);
            }
        }
    }
}
