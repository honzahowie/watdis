﻿
namespace GrafikaGUI
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tvarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kružniceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.elipsaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.čtverecToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barvaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bíláToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.žlutáToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.červenáToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zelenáToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modráToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.černáToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonVykresli = new System.Windows.Forms.Button();
            this.buttonVymaz = new System.Windows.Forms.Button();
            this.textBoxTloustka = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tvarToolStripMenuItem,
            this.barvaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(428, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tvarToolStripMenuItem
            // 
            this.tvarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kružniceToolStripMenuItem,
            this.elipsaToolStripMenuItem,
            this.čtverecToolStripMenuItem});
            this.tvarToolStripMenuItem.Name = "tvarToolStripMenuItem";
            this.tvarToolStripMenuItem.Size = new System.Drawing.Size(50, 24);
            this.tvarToolStripMenuItem.Text = "Tvar";
            // 
            // kružniceToolStripMenuItem
            // 
            this.kružniceToolStripMenuItem.Name = "kružniceToolStripMenuItem";
            this.kružniceToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.kružniceToolStripMenuItem.Text = "Kružnice";
            this.kružniceToolStripMenuItem.Click += new System.EventHandler(this.kružniceToolStripMenuItem_Click);
            // 
            // elipsaToolStripMenuItem
            // 
            this.elipsaToolStripMenuItem.Name = "elipsaToolStripMenuItem";
            this.elipsaToolStripMenuItem.Size = new System.Drawing.Size(148, 26);
            this.elipsaToolStripMenuItem.Text = "Elipsa";
            // 
            // čtverecToolStripMenuItem
            // 
            this.čtverecToolStripMenuItem.Name = "čtverecToolStripMenuItem";
            this.čtverecToolStripMenuItem.Size = new System.Drawing.Size(148, 26);
            this.čtverecToolStripMenuItem.Text = "Čtverec";
            // 
            // barvaToolStripMenuItem
            // 
            this.barvaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bíláToolStripMenuItem,
            this.žlutáToolStripMenuItem,
            this.červenáToolStripMenuItem,
            this.zelenáToolStripMenuItem,
            this.modráToolStripMenuItem,
            this.černáToolStripMenuItem});
            this.barvaToolStripMenuItem.Name = "barvaToolStripMenuItem";
            this.barvaToolStripMenuItem.Size = new System.Drawing.Size(60, 24);
            this.barvaToolStripMenuItem.Text = "Barva";
            // 
            // bíláToolStripMenuItem
            // 
            this.bíláToolStripMenuItem.Name = "bíláToolStripMenuItem";
            this.bíláToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.bíláToolStripMenuItem.Text = "Bílá";
            // 
            // žlutáToolStripMenuItem
            // 
            this.žlutáToolStripMenuItem.Name = "žlutáToolStripMenuItem";
            this.žlutáToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.žlutáToolStripMenuItem.Text = "Žlutá";
            // 
            // červenáToolStripMenuItem
            // 
            this.červenáToolStripMenuItem.Name = "červenáToolStripMenuItem";
            this.červenáToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.červenáToolStripMenuItem.Text = "Červená";
            // 
            // zelenáToolStripMenuItem
            // 
            this.zelenáToolStripMenuItem.Name = "zelenáToolStripMenuItem";
            this.zelenáToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.zelenáToolStripMenuItem.Text = "Zelená";
            // 
            // modráToolStripMenuItem
            // 
            this.modráToolStripMenuItem.Name = "modráToolStripMenuItem";
            this.modráToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.modráToolStripMenuItem.Text = "Modrá";
            // 
            // černáToolStripMenuItem
            // 
            this.černáToolStripMenuItem.Name = "černáToolStripMenuItem";
            this.černáToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.černáToolStripMenuItem.Text = "Černá";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(102, 31);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 300);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // buttonVykresli
            // 
            this.buttonVykresli.Location = new System.Drawing.Point(13, 32);
            this.buttonVykresli.Name = "buttonVykresli";
            this.buttonVykresli.Size = new System.Drawing.Size(75, 23);
            this.buttonVykresli.TabIndex = 2;
            this.buttonVykresli.Text = "Vykresli";
            this.buttonVykresli.UseVisualStyleBackColor = true;
            this.buttonVykresli.Click += new System.EventHandler(this.buttonVykresli_Click);
            // 
            // buttonVymaz
            // 
            this.buttonVymaz.Location = new System.Drawing.Point(13, 62);
            this.buttonVymaz.Name = "buttonVymaz";
            this.buttonVymaz.Size = new System.Drawing.Size(75, 23);
            this.buttonVymaz.TabIndex = 3;
            this.buttonVymaz.Text = "Vymazat";
            this.buttonVymaz.UseVisualStyleBackColor = true;
            this.buttonVymaz.Click += new System.EventHandler(this.buttonVymaz_Click);
            // 
            // textBoxTloustka
            // 
            this.textBoxTloustka.Location = new System.Drawing.Point(13, 92);
            this.textBoxTloustka.Name = "textBoxTloustka";
            this.textBoxTloustka.Size = new System.Drawing.Size(75, 22);
            this.textBoxTloustka.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Tloušťka pera";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 355);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxTloustka);
            this.Controls.Add(this.buttonVymaz);
            this.Controls.Add(this.buttonVykresli);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tvarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kružniceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem elipsaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem čtverecToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem barvaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bíláToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem žlutáToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem červenáToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zelenáToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modráToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem černáToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonVykresli;
        private System.Windows.Forms.Button buttonVymaz;
        private System.Windows.Forms.TextBox textBoxTloustka;
        private System.Windows.Forms.Label label1;
    }
}

