﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sifrovani2
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader input = new StreamReader("in.txt");
            string text = input.ReadToEnd();
            Console.WriteLine("Vstup: " + text);

            Console.Write("Zadej posun: ");
            int posun = Convert.ToInt32(Console.ReadLine());

            string vystup = "";
            foreach (char znakVstup in text)
            {
                int pozicePismena = (int)znakVstup;
                int novaPozicePismena = pozicePismena + posun;
                char znakVystup = (char)novaPozicePismena;
                vystup = vystup + znakVystup;
            }

            Console.WriteLine("Výstup: " + vystup);
            StreamWriter output = new StreamWriter("out.txt");
            output.Write(vystup);
            output.Close();


            Console.ReadKey();
        }
    }
}
