﻿
namespace Runner
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelScore = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBoxLarge = new System.Windows.Forms.PictureBox();
            this.pictureBoxSmall = new System.Windows.Forms.PictureBox();
            this.pictureBoxFloor = new System.Windows.Forms.PictureBox();
            this.pictureBoxDino = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFloor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDino)).BeginInit();
            this.SuspendLayout();
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.BackColor = System.Drawing.Color.Transparent;
            this.labelScore.Font = new System.Drawing.Font("Century Gothic", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelScore.Location = new System.Drawing.Point(13, 13);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(125, 34);
            this.labelScore.TabIndex = 2;
            this.labelScore.Text = "Score: 0";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 20;
            this.timer1.Tick += new System.EventHandler(this.MainGameTimerEvent);
            // 
            // pictureBoxLarge
            // 
            this.pictureBoxLarge.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxLarge.Image = global::Runner.Properties.Resources.obstacleLarge;
            this.pictureBoxLarge.Location = new System.Drawing.Point(321, 395);
            this.pictureBoxLarge.Name = "pictureBoxLarge";
            this.pictureBoxLarge.Size = new System.Drawing.Size(105, 60);
            this.pictureBoxLarge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLarge.TabIndex = 1;
            this.pictureBoxLarge.TabStop = false;
            this.pictureBoxLarge.Tag = "obstacle";
            // 
            // pictureBoxSmall
            // 
            this.pictureBoxSmall.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxSmall.Image = global::Runner.Properties.Resources.obstacleSmall;
            this.pictureBoxSmall.Location = new System.Drawing.Point(215, 383);
            this.pictureBoxSmall.Name = "pictureBoxSmall";
            this.pictureBoxSmall.Size = new System.Drawing.Size(42, 72);
            this.pictureBoxSmall.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxSmall.TabIndex = 1;
            this.pictureBoxSmall.TabStop = false;
            this.pictureBoxSmall.Tag = "obstacle";
            // 
            // pictureBoxFloor
            // 
            this.pictureBoxFloor.BackColor = System.Drawing.Color.Black;
            this.pictureBoxFloor.Location = new System.Drawing.Point(-5, 455);
            this.pictureBoxFloor.Name = "pictureBoxFloor";
            this.pictureBoxFloor.Size = new System.Drawing.Size(2760, 50);
            this.pictureBoxFloor.TabIndex = 0;
            this.pictureBoxFloor.TabStop = false;
            // 
            // pictureBoxDino
            // 
            this.pictureBoxDino.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxDino.Image = global::Runner.Properties.Resources.dinoRun;
            this.pictureBoxDino.Location = new System.Drawing.Point(19, 325);
            this.pictureBoxDino.Name = "pictureBoxDino";
            this.pictureBoxDino.Size = new System.Drawing.Size(144, 129);
            this.pictureBoxDino.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDino.TabIndex = 1;
            this.pictureBoxDino.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1009, 498);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.pictureBoxLarge);
            this.Controls.Add(this.pictureBoxSmall);
            this.Controls.Add(this.pictureBoxFloor);
            this.Controls.Add(this.pictureBoxDino);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Dino Runner";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keyIsDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyIsUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFloor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDino)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxFloor;
        private System.Windows.Forms.PictureBox pictureBoxDino;
        private System.Windows.Forms.PictureBox pictureBoxSmall;
        private System.Windows.Forms.PictureBox pictureBoxLarge;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.Timer timer1;
    }
}

