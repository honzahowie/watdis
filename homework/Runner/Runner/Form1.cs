﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Runner
{
    public partial class Form1 : Form
    {        
        Random rnd = new Random();
        int position;
        bool isGameOver = false;
        GameEngine gEngine = new GameEngine();


        public Form1()
        {
            InitializeComponent();

            GameReset();
        }

        private void MainGameTimerEvent(object sender, EventArgs e)
        {            
            int score = gEngine.HandleSingleMove(pictureBoxDino, ClientSize.Width, this.Controls);
                        
            if (score == -1)
            {
                timer1.Stop();
                pictureBoxDino.Image = Properties.Resources.dinoDeath;
                labelScore.Text += " Stiskněte R pro restartování hry nebo ESC pro ukončení.";
                isGameOver = true;
            }
            else
            {
                labelScore.Text = "Skóre: " + score;
            }
                        
        }

        private void keyIsDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space && gEngine.jumping == false)
            {
                gEngine.jumping = true;
            }
            if (e.KeyCode == Keys.Up && gEngine.jumping == true)
            {
                pictureBoxDino.Top = 175;
            }
        }

        private void keyIsUp(object sender, KeyEventArgs e)
        {
            if (gEngine.jumping == true)
            {
                gEngine.jumping = false;
            }

            if (e.KeyCode == Keys.R && isGameOver == true)
            {
                GameReset();
            }

            if (e.KeyCode == Keys.Escape && isGameOver == true)
            {
                Application.Exit();
            }
        }
        private void GameReset()
        {
            position = gEngine.GameReset(this.Controls, this.ClientSize.Width);
            labelScore.Text = "Skóre: " + gEngine.score;
            pictureBoxDino.Image = Properties.Resources.dinoRun;
            isGameOver = false;
            pictureBoxDino.Top = 269;
            this.BackColor = Color.White;


            timer1.Start();
        }
    }
}
