﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.Control;

namespace Runner
{
    class GameEngine
    {
        public bool jumping = false;
        private int jumpSpeed;
        private int force = 15;
        public int score = 0;
        private int obstacleSpeed = 12;
        private Random rnd = new Random();

        //public GameEngine(int obstacleSpeed)
        //{
        //    this.obstacleSpeed = obstacleSpeed;


        //}

        public int HandleSingleMove(PictureBox pictureBoxDino, int width, ControlCollection controls)
        {
            pictureBoxDino.Top += jumpSpeed;

            
            if (jumping == true && force < 0)
            {
                jumping = false;
            }

            if (jumping == true)
            {
                jumpSpeed = -15;
                force -= 1;
            }
            else
            {
                jumpSpeed = 15;
            }

            if (pictureBoxDino.Top > 268 && jumping == false)
            {
                force = 15;
                pictureBoxDino.Top = 269;
                jumpSpeed = 0;
            }


            foreach (Control x in controls)
            {
                if (x is PictureBox && (string)x.Tag == "obstacle")
                {
                    x.Left -= obstacleSpeed;

                    if (x.Left < -100)
                    {
                        x.Left = width + rnd.Next(500, 800) + (x.Width * 20);
                        score++;
                    }

                    if (pictureBoxDino.Bounds.IntersectsWith(x.Bounds))
                    {
                        return -1;
                    }
                }
            }
            if (score > 10)
            {
                obstacleSpeed = 15;
            }

            if (score >= 20)
            {                
                obstacleSpeed = 18;
            }
            if (score >= 50)
            {                
                obstacleSpeed = 21;
            }

            return score;
        }

        public int GameReset(ControlCollection controls, int width)
        {
            force = 15;
            jumpSpeed = 0;
            jumping = false;
            score = 0;
            obstacleSpeed = 12;

            int position = 0;
            foreach (Control x in controls)
            {
                if (x is PictureBox && (string)x.Tag == "obstacle")
                {
                    position = width + rnd.Next(500, 800) + (x.Width * 10);

                    x.Left = position;
                }
            }
            return position;
        }
    }
}
