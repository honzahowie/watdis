﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KvadratickaRovnice
{
    class Program
    {
        static void Main(string[] args)
        {
            double x1;
            double x2;

            Console.WriteLine("Zadej a:");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Zadej b:");
            double b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Zadej c:");
            double c = Convert.ToDouble(Console.ReadLine());

            double d = b * b - 4 * a * c;
            if (d > 0)
            {
                x1 = (-b + (double)Math.Sqrt(d)) / (2 * a);
                x2 = (-b - (double)Math.Sqrt(d)) / (2 * a);
                Console.WriteLine("Kvadratická rovnice {0} * x^2 + ({1}) * x + ({2}) = 0 má dvě řešení: x1 = {3} a x2 = {4}", a, b, c, x1, x2);
            }
            else if (d == 0)
            {
                x1 = -b / (2 * a);
                Console.WriteLine("Kvadratická rovnice {0} * x^2 + ({1}) * x + ({2}) = 0 má jedno řešení: x1 = {3}", a, b, c, x1);
            }
            else if (d < 0)
            {
                Console.WriteLine("Kvadratická rovnice {0} * x^2 + ({1}) * x + ({2}) = 0 nemá řešení", a, b, c);
            }
                       
            Console.ReadKey();
        }
    }
}
