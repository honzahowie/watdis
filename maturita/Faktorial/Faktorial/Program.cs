﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Faktorial
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Zadejte celé kladné číslo");
            ulong vstup = ulong.Parse(Console.ReadLine());

            BigInteger vysledekRek = new BigInteger();
            vysledekRek = FaktorialRek(vstup);
            Console.WriteLine("Výsledek rekurze je " + vysledekRek);

            BigInteger vysledekFor = new BigInteger();
            vysledekFor = 1;
            for (ulong i = vstup; i >= 2; i--)
            {
                vysledekFor = BigInteger.Multiply(vysledekFor, i);  
            }
            Console.WriteLine("Výsledek cyklu je {0}", vysledekFor);
            
            Console.ReadKey();
        }

        static BigInteger FaktorialRek(ulong vstup)
        {
            if (vstup <= 1)
            {
                return 1;
            }
            return vstup * FaktorialRek(vstup - 1);
        }        
    }
}
