﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pole
{
    class Program
    {
        static void Main(string[] args)
        {
            //vytvoření a naplnění pole
            int[] pole = new int[10];
            for (int i = 0; i < 10; i++)
            {
                pole[i] = i + 1;
            }
            //vypsání pole
            for (int i = 0; i < pole.Length; i++)
            {
                Console.Write(pole[i] + " ");
            }
            Console.WriteLine();

            //pole s proměnnou délkou a vlastními čísly
            Console.Write("Zadejte počet čísel v poli: ");
            int pocet = Convert.ToInt32(Console.ReadLine());

            int[] pole2 = new int[pocet];
            for (int i = 0; i < pocet; i++)
            {
                Console.Write("Zadejte číslo na indexu " + i + ": ");
                pole2[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("Průměr čísel v poli: " + pole2.Average());

            //řazení podle abecedy (Array.Sort)
            Console.Write("Zadejte počet jmen v poli: ");
            int pocet2 = Convert.ToInt32(Console.ReadLine());

            string[] pole3 = new string[pocet2];
            for (int i = 0; i < pocet2; i++)
            {
                Console.Write("Zadejte jméno na indexu " + i + ": ");
                pole3[i] = Console.ReadLine();
            }
            
            Array.Sort(pole3);
            Console.Write("Seřazená jména podle abecedy: ");
            foreach (string item in pole3)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();

            //pozice prvků v poli (Array.IndexOf)
            Console.Write("Zadejte počet jmen v poli: ");
            int pocet3 = Convert.ToInt32(Console.ReadLine());

            string[] pole4 = new string[pocet3];
            for (int i = 0; i < pocet3; i++)
            {
                Console.Write("Zadejte {0}. jméno: ", i + 1);
                pole4[i] = Console.ReadLine();
            }

            znovu:
            Console.Write("Zadejte jméno z pole: ");            
            string jmeno = Console.ReadLine();
            int pozice = Array.IndexOf(pole4, jmeno);

            if (pozice >= 0)
            {
                Console.WriteLine("Jméno {0} je na {1}. pozici", jmeno, pozice + 1);
            }
            else
            {
                Console.WriteLine("Jméno není v poli.");
                goto znovu;
            }


            Console.ReadKey();
        }
    }
}
