﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prvocisla
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Zadejte číslo pro kontrolu: ");
            int vstup = Convert.ToInt32(Console.ReadLine());
            
            bool jeDelitelne = false;

            for (int delitel = 2; delitel < vstup; delitel++)
            {
                if (vstup % delitel == 0)
                {
                    jeDelitelne = true;
                }
            }

            if (jeDelitelne == false)
            {
                Console.WriteLine("Číslo " + vstup + " je prvočíslo");
            }
            else
            {
                Console.WriteLine("Číslo " + vstup + " není prvočíslo");
            }
            
            Console.ReadKey();            
        }
    }
}
