﻿
namespace PoleForms
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxVstup = new System.Windows.Forms.TextBox();
            this.textBoxPole = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.richTextBoxVystup = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Zadej maximálně 10 čísel";
            // 
            // textBoxVstup
            // 
            this.textBoxVstup.Location = new System.Drawing.Point(185, 13);
            this.textBoxVstup.Name = "textBoxVstup";
            this.textBoxVstup.Size = new System.Drawing.Size(52, 22);
            this.textBoxVstup.TabIndex = 1;
            this.textBoxVstup.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxVstup_KeyDown);
            // 
            // textBoxPole
            // 
            this.textBoxPole.Location = new System.Drawing.Point(16, 41);
            this.textBoxPole.Name = "textBoxPole";
            this.textBoxPole.Size = new System.Drawing.Size(221, 22);
            this.textBoxPole.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 70);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(221, 73);
            this.button1.TabIndex = 3;
            this.button1.Text = "Zjisti počet výskytů minima";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // richTextBoxVystup
            // 
            this.richTextBoxVystup.Location = new System.Drawing.Point(16, 150);
            this.richTextBoxVystup.Name = "richTextBoxVystup";
            this.richTextBoxVystup.Size = new System.Drawing.Size(221, 96);
            this.richTextBoxVystup.TabIndex = 4;
            this.richTextBoxVystup.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(253, 264);
            this.Controls.Add(this.richTextBoxVystup);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxPole);
            this.Controls.Add(this.textBoxVstup);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxVstup;
        private System.Windows.Forms.TextBox textBoxPole;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTextBoxVystup;
    }
}

