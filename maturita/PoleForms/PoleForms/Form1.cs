﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PoleForms
{
    public partial class Form1 : Form
    {        
        public Form1()
        {
            InitializeComponent();            
        }

        int[] pole = new int[10];
        int index = 0;
        
        private void textBoxVstup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                pole[index] = Convert.ToInt32(textBoxVstup.Text);
                index++;

                textBoxPole.Text += textBoxVstup.Text + " ";
                textBoxVstup.Text = "";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int min = pole[0];
            for (int i = 1; i < index; i++)
            {
                if (pole[i] < min)
                {
                    min = pole[i];
                }
            }

            int pocetMin = 0;
            for (int i = 0; i < index; i++)
            {
                if (pole[i] == min)
                {
                    pocetMin++;
                }
            }

            richTextBoxVystup.Text = "Nejmenší číslo je " + Convert.ToString(min) + Environment.NewLine + "Počet výskytů je " + Convert.ToString(pocetMin);
            
        }
    }
}
