﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Zadejte poslední člen posloupnosti: ");
            int vstup = Convert.ToInt32(Console.ReadLine());
            
            ulong a1 = 0;
            ulong a2 = 1;
            Console.WriteLine(a1);
            Console.WriteLine(a2);

            for (int i = 2; i <= vstup; i++)
            {
                ulong a3 = a1 + a2;
                Console.WriteLine(a3);
                a1 = a2;
                a2 = a3; 
            }
            
            Console.WriteLine("Výsledek rekurze: " + FiboRek(vstup));
            
            Console.ReadKey();
        }

        public static int FiboRek(int x)
        {            
            int vysledek;
            if (x <=1)
            {
                return x;
            }
            else
            {
                vysledek = FiboRek(x - 1) + FiboRek(x - 2);                
            }
            return vysledek;
        }
    }
}
