﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalkulacka
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Zdravím, vítejte v kalkulačce.");

            string pokracovat = "Y";
            while (pokracovat == "Y")
            {

                Console.Write("Zadejte první číslo: ");
                float a = float.Parse(Console.ReadLine());
                Console.Write("Zadejte druhé číslo: ");
                float b = float.Parse(Console.ReadLine());

                while (pokracovat == "Y")
                {
                    Console.WriteLine("Vyberte operaci: 1 - sčítání; 2 - odčítání; 3 - násobení; 4 - dělení");
                    int operace = Convert.ToInt32(Console.ReadLine());
                    float vysledek = 0;

                    switch (operace)
                    {
                        case 1:
                            vysledek = a + b;
                            break;
                        case 2:
                            vysledek = a - b;
                            break;
                        case 3:
                            vysledek = a * b;
                            break;
                        case 4:
                            vysledek = a / b;
                            break;
                    }
                    if (operace >= 1 && operace <= 4)
                    {
                        Console.WriteLine("Výsledek je " + vysledek);                        
                        pokracovat = "N";
                    }
                    else
                    {
                        Console.WriteLine("Neplatná volba. Přejete si vybrat operaci znovu? Y/N");
                        pokracovat = Console.ReadLine();                        
                    }                    
                }
                Console.WriteLine("Přejete si zadat nový příklad? Y/N");
                pokracovat = Console.ReadLine();
            }
            
            Console.WriteLine("Děkuji za použití kalkulačky. Stisknutím libovolné klávesy program ukončíte.");
            Console.ReadKey();
        }
    }
}
