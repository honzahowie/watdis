﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PoleForms2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int[] pole = new int[10];            
            Random rnd = new Random();
            for (int i = 0; i < 10; i++)
            {
                pole[i] = rnd.Next(0, 100);
                textBoxVstup.Text += Convert.ToString(pole[i]) + " ";
            }

            int pocetVetsich = 0;
            foreach (var item in pole)
            {
                if (item > pole.Average())
                {
                    pocetVetsich++;
                }
            }

            richTextBoxVystup.Text = "Průměr čísel je " + Convert.ToString(pole.Average()) + Environment.NewLine + "Větších než průměr je " + Convert.ToString(pocetVetsich) + " čísel";
        }
    }
}
